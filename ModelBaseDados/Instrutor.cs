﻿namespace ModelBaseDados
{
    public class Instrutor : Pessoa
    {
        public DateTime Desde { get; set; }
        public string Tecnologia { get; set; }
    }
}
