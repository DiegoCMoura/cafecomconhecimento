﻿// See https://aka.ms/new-console-template for more information
using Sequences.Data;

Sequences();

static void Sequences()
{
    using var db = new ApplicationContext();
    db.Database.EnsureDeleted();
    db.Database.EnsureCreated();
}