﻿// See https://aka.ms/new-console-template for more information
using Microsoft.EntityFrameworkCore;
using PropertyBags.Data;

PacotesDePropriedades();

static void PacotesDePropriedades()
{
    using (var db = new ApplicationContext())
    {
        db.Database.EnsureDeleted();
        db.Database.EnsureCreated();

        var configuracao = new Dictionary<string, object>
        {
            ["Chave"] = "SenhaBancoDeDados",
            ["Valor"] = Guid.NewGuid().ToString()
        };

        db.Configuracoes.Add(configuracao);
        db.SaveChanges();

        var configuracoes = db
            .Configuracoes
            .AsNoTracking()
            .Where(p => p["Chave"] == "SenhaBancoDeDados")
            .ToArray();

        foreach (var dic in configuracoes)
        {
            Console.WriteLine($"Chave: {dic["Chave"]} - Valor: {dic["Valor"]}");
        }
    }
}