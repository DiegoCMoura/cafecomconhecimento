﻿// See https://aka.ms/new-console-template for more information
using Microsoft.EntityFrameworkCore;
using ModelBaseDados;
using TPT.Data;

ExemploTPT();

static void ExemploTPT()
{
    using (var db = new ApplicationContext())
    {
        db.Database.EnsureDeleted();
        db.Database.EnsureCreated();

        var pessoa = new Pessoa { Nome = "Fulano de Tal" };

        var instrutor = new Instrutor { Nome = "Diego Camara", Tecnologia = ".NET", Desde = DateTime.Now };

        var aluno = new Aluno { Nome = "Francisco", Idade = 31, DataContrato = DateTime.Now.AddDays(-1) };

        db.AddRange(pessoa, instrutor, aluno);
        db.SaveChanges();

        var pessoas = db.Pessoas.AsNoTracking().ToArray();
        var instrutores = db.Instrutores.AsNoTracking().ToArray();
        var alunos = db.Alunos.AsNoTracking().ToArray();

        Console.WriteLine("Pessoas **************");
        foreach (var p in pessoas)
        {
            Console.WriteLine($"Id: {p.Id} -> {p.Nome}");
        }

        Console.WriteLine("Instrutores **************");
        foreach (var p in instrutores)
        {
            Console.WriteLine($"Id: {p.Id} -> {p.Nome}, Tecnologia: {p.Tecnologia}, Desde: {p.Desde}");
        }

        Console.WriteLine("Alunos **************");
        foreach (var p in alunos)
        {
            Console.WriteLine($"Id: {p.Id} -> {p.Nome}, Idade: {p.Idade}, Data do Contrato: {p.DataContrato}");
        }
    }
}