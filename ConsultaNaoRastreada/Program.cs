﻿// See https://aka.ms/new-console-template for more information
using Consulta.Data;
using Microsoft.EntityFrameworkCore;
using ModelBase;

ConsultaNaoRastreada();

static void ConsultaNaoRastreada()
{
    using var db = new ApplicationContext();
    Setup(db);

    var funcionarios = db.Funcionarios.AsNoTracking().Include(p => p.Departamento).ToList();
}

static void Setup(ApplicationContext db)
{
    db.Database.EnsureDeleted();
    db.Database.EnsureCreated();

    db.Departamentos.Add(new Departamento
    {
        Descricao = "Departamento Teste",
        Ativo = true,
        Funcionarios = Enumerable.Range(1, 100).Select(p => new Funcionario
        {
            CPF = p.ToString().PadLeft(11, '0'),
            Nome = $"Funcionando {p}",
            RG = p.ToString()
        }).ToList()
    });

    db.SaveChanges();
}