﻿// See https://aka.ms/new-console-template for more information
using Microsoft.EntityFrameworkCore;
using ModelBaseDados;
using Relacionamento1xN.Data;

Relacionamento1ParaN();

static void Relacionamento1ParaN()
{
    using (var db = new ApplicationContext())
    {
        db.Database.EnsureDeleted();
        db.Database.EnsureCreated();

        var estado = new Estado
        {
            Nome = "Ceara",
            Governador = new Governador { Nome = "Diego Camara" }
        };

        estado.Cidades.Add(new Cidade { Nome = "Fortaleza" });

        db.Estados.Add(estado);

        db.SaveChanges();
    }

    using (var db = new ApplicationContext())
    {
        var estados = db.Estados.ToList();

        estados[0].Cidades.Add(new Cidade { Nome = "Aracaju" });

        db.SaveChanges();

        foreach (var est in db.Estados.Include(p => p.Cidades).AsNoTracking())
        {
            Console.WriteLine($"Estado: {est.Nome}, Governador: {est.Governador.Nome}");

            foreach (var cidade in est.Cidades)
            {
                Console.WriteLine($"\t Cidade: {cidade.Nome}");
            }
        }
    }
}