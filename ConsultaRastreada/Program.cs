﻿// See https://aka.ms/new-console-template for more information
using ConsultaRastreada.Data;
using Microsoft.EntityFrameworkCore;
using ModelBase;

//Setup();
ConsultaRastreada();

static void ConsultaRastreada()
{
    using var db = new ApplicationContext();
    Setup(db);

    var funcionarios = db.Funcionarios.Include(p => p.Departamento).ToList();
}

static void Setup(ApplicationContext db)
{
    db.Database.EnsureDeleted();
    db.Database.EnsureCreated();

    db.Departamentos.Add(new Departamento
    {
        Descricao = "Departamento Teste",
        Ativo = true,
        Funcionarios = Enumerable.Range(1, 100).Select(p => new Funcionario {
            CPF = p.ToString().PadLeft(11, '0'),
            Nome = $"Funcionando {p}",
            RG = p.ToString()
        }).ToList()
    });

    db.SaveChanges();
}