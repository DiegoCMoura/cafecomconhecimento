﻿// See https://aka.ms/new-console-template for more information
using Microsoft.EntityFrameworkCore;
using ModelBaseDados;
using Relacionamento1x1.Data;

Relacionamento1Para1();

static void Relacionamento1Para1()
{
    using var db = new ApplicationContext();
    db.Database.EnsureDeleted();
    db.Database.EnsureCreated();

    var estado = new Estado {
        Nome = "Ceará",
        Governador = new Governador { Nome = "Diego Câmara" }
    };

    db.Estados.Add(estado);

    db.SaveChanges();

    var estados = db.Estados.AsNoTracking().ToList();

    estados.ForEach(est => {
        Console.WriteLine($"Estado: {est.Nome}, Governador: {est.Governador.Nome}");
    });
}