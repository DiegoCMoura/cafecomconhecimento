﻿// See https://aka.ms/new-console-template for more information
using Microsoft.EntityFrameworkCore;
using ShadowProperties.Data;

PropriedadesDeSombra();
//TrabalhandoComPropriedadesDeSombra();

static void PropriedadesDeSombra()
{
    using var db = new ApplicationContext();
    db.Database.EnsureDeleted();
    db.Database.EnsureCreated();
}

static void TrabalhandoComPropriedadesDeSombra()
{
    using var db = new ApplicationContext();
    /*db.Database.EnsureDeleted();
    db.Database.EnsureCreated();

    var departamento = new Departamento
    {
        Descricao = "Departamento - Propriedade de Sombra"
    };

    db.Departamentos.Add(departamento);

    db.Entry(departamento).Property("UltimaAtualizacao").CurrentValue = DateTime.Now;

    db.SaveChanges();
    */

    var departamentos = db.Departamentos.Where(p => EF.Property<DateTime>(p, "UltimaAtualizacao") < DateTime.Now).ToArray();
}