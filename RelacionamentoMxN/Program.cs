﻿// See https://aka.ms/new-console-template for more information
using Microsoft.EntityFrameworkCore;
using ModelBaseDados;
using RelacionamentoMxN.Data;

RelacionamentoMxN();

static void RelacionamentoMxN()
{
    using (var db = new ApplicationContext())
    {
        db.Database.EnsureDeleted();
        db.Database.EnsureCreated();

        var ator1 = new Ator { Nome = "Diego" };
        var ator2 = new Ator { Nome = "João" };
        var ator3 = new Ator { Nome = "Maria" };

        var filme1 = new Filme { Descricao = "A volta dos que não foram" };
        var filme2 = new Filme { Descricao = "De volta para o futuro" };
        var filme3 = new Filme { Descricao = "Poeira em alto mar filme" };

        ator1.Filmes.Add(filme1);
        ator1.Filmes.Add(filme2);

        ator2.Filmes.Add(filme1);

        filme3.Atores.Add(ator1);
        filme3.Atores.Add(ator2);
        filme3.Atores.Add(ator3);

        db.AddRange(ator1, ator2, filme3);

        db.SaveChanges();

        foreach (var ator in db.Atores.Include(e => e.Filmes))
        {
            Console.WriteLine($"Ator: {ator.Nome}");

            foreach (var filme in ator.Filmes)
            {
                Console.WriteLine($"\tFilme: {filme.Descricao}");
            }
        }
    }
}