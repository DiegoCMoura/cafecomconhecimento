﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Microsoft.Extensions.Logging;
using ModelBaseDados;

namespace ConversorValores.Data
{
    public class ApplicationContext: DbContext
    {
        public DbSet<Departamento> Departamentos { get; set; }
        public DbSet<Funcionario> Funcionarios { get; set; }
        public DbSet<Conversor> Conversores { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            const string strConnection = "Data source=(localdb)\\mssqllocaldb; Initial Catalog=Cafe_ModeloDados;Integrated Security=true;pooling=true;";
            optionsBuilder
                .UseSqlServer(strConnection)
                .EnableSensitiveDataLogging()
                .LogTo(Console.WriteLine, LogLevel.Information);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //var conversao = new ValueConverter<Versao, string>(p => p.ToString(), p => (Versao)Enum.Parse(typeof(Versao), p));
            //var conversao1 = new EnumToStringConverter<Versao>();

            modelBuilder.Entity<Conversor>()
                 .Property(p => p.Versao)
                 .HasConversion<string>();
                //.HasConversion(p => p.ToString(), p => (Versao)Enum.Parse(typeof(Versao), p));
                //.HasConversion(conversao);
                //.HasConversion(conversao1);

            modelBuilder.Entity<Conversor>()
                .Property(p => p.Status)
                .HasConversion(new Conversores.ConversorCustomizado());
        }
    }
}
