﻿// See https://aka.ms/new-console-template for more information
using ConversorValores.Data;
using Microsoft.EntityFrameworkCore;
using ModelBaseDados;

Conversor();
//ConversorCustomizado();

static void Conversor()
{
    using var db = new ApplicationContext();

    var script = db.Database.GenerateCreateScript();

    Console.WriteLine(script);
}

static void ConversorCustomizado()
{
    using var db = new ApplicationContext();
    db.Database.EnsureDeleted();
    db.Database.EnsureCreated();

    db.Conversores.Add(
        new Conversor {
            Status = Status.Devolvido,
        });

    db.SaveChanges();

    var conversorEmAnalise = db.Conversores.AsNoTracking().FirstOrDefault(p => p.Status == Status.Analise);

    var conversorDevolvido = db.Conversores.AsNoTracking().FirstOrDefault(p => p.Status == Status.Devolvido);
}