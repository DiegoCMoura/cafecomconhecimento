﻿// See https://aka.ms/new-console-template for more information
using Collations.Data;

Collations();

static void Collations()
{
    using var db = new ApplicationContext();
    db.Database.EnsureDeleted();
    db.Database.EnsureCreated();
}
