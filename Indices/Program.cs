﻿// See https://aka.ms/new-console-template for more information
using Indices.Data;

Indices();

static void Indices()
{
    using var db = new ApplicationContext();
    db.Database.EnsureDeleted();
    db.Database.EnsureCreated();
}