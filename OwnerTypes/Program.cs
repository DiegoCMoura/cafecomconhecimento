﻿// See https://aka.ms/new-console-template for more information
using Microsoft.EntityFrameworkCore;
using ModelBaseDados;
using OwnerTypes.Data;

TiposDePropriedades();

static void TiposDePropriedades()
{
    using var db = new ApplicationContext();
    db.Database.EnsureDeleted();
    db.Database.EnsureCreated();

    var cliente = new Cliente
    {
        Nome = "Fulano de tal",
        Telefone = "(79) 98888-9999",
        Endereco = new Endereco { Bairro = "Centro", Cidade = "Sao Paulo", Estado = "SP", Logradouro = "Av. Paulista", Numero = "10" }
    };

    db.Clientes.Add(cliente);

    db.SaveChanges();

    var clientes = db.Clientes.AsNoTracking().ToList();

    var options = new System.Text.Json.JsonSerializerOptions { WriteIndented = true };

    clientes.ForEach(cli =>
    {
        var json = System.Text.Json.JsonSerializer.Serialize(cli, options);

        Console.WriteLine(json);
    });
}