﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ModelBaseDados;

namespace OwnerTypes.Configurations
{
    public class ClienteConfiguration : IEntityTypeConfiguration<Cliente>
    {
        public void Configure(EntityTypeBuilder<Cliente> builder)
        {
            builder.OwnsOne(p => p.Endereco);

            //builder.OwnsOne(x => x.Endereco, end =>
            //{
            //    end.Property(p => p.Bairro).HasColumnName("Bairro");

            //    end.ToTable("Enderecos");
            //});
        }
    }
}
