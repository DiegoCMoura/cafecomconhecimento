﻿// See https://aka.ms/new-console-template for more information
using Consulta.Data;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using ModelBase;

ConsultaParametrizada();

static void ConsultaParametrizada()
{
    using var db = new ApplicationContext();
    Setup(db);

    var id = new SqlParameter
    {
        Value = 1,
        SqlDbType = System.Data.SqlDbType.Int
    };
    var departamentos = db.Departamentos
        .FromSqlRaw("SELECT * FROM Departamentos WHERE Id>{0}", id)
        .Where(p => p.Ativo)
        .ToList();

    foreach (var departamento in departamentos)
    {
        Console.WriteLine($"Descrição: {departamento.Descricao}");
    }
}

static void Setup(ApplicationContext db)
{
    if (db.Database.EnsureCreated())
    {
        db.Departamentos.AddRange(
            new Departamento
            {
                Ativo = true,
                Descricao = "Departamento 01",
                Funcionarios = new List<Funcionario>
                {
                            new Funcionario
                            {
                                Nome = "Funcionario 01",
                                CPF = "99999999911",
                                RG= "2100062"
                            }
                }
            },
            new Departamento
            {
                Ativo = true,
                Descricao = "Departamento 02",
                Funcionarios = new List<Funcionario>
                {
                            new Funcionario
                            {
                                Nome = "Funcionario 02",
                                CPF = "88888888811",
                                RG= "3100062"
                            },
                            new Funcionario
                            {
                                Nome = "Funcionario 03",
                                CPF = "77777777711",
                                RG= "1100062"
                            }
                }
            });

        db.SaveChanges();
        db.ChangeTracker.Clear();
    }
}