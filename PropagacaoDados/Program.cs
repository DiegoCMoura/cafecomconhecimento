﻿using Microsoft.EntityFrameworkCore;
using PropagacaoDados.Data;

PropagarDados();

static void PropagarDados()
{
    using var db = new ApplicationContext();
    db.Database.EnsureDeleted();
    db.Database.EnsureCreated();

    var script = db.Database.GenerateCreateScript();
    Console.WriteLine(script);
}