﻿// See https://aka.ms/new-console-template for more information
using CarregamentoAdiantado.Data;
using Microsoft.EntityFrameworkCore;
using ModelBase;

Carregamento();

static void Carregamento()
{
    using var db = new ApplicationContext();
    Setup(db);

    var departamentos = db
                .Departamentos
                .Include(p => p.Funcionarios);

    foreach (var departamento in departamentos)
    {
        Console.WriteLine("---------------------------------------");
        Console.WriteLine($"Departamento: {departamento.Descricao}");

        if (departamento.Funcionarios?.Any() ?? false)
        {
            foreach (var funcionario in departamento.Funcionarios)
            {
                Console.WriteLine($"\tFuncionario: {funcionario.Nome}");
            }
        }
        else
        {
            Console.WriteLine($"\tNenhum funcionario encontrado!");
        }
    }
}

static void Setup(ApplicationContext db)
{
    db.Database.EnsureDeleted();
    db.Database.EnsureCreated();

    if (!db.Departamentos.Any())
    {
        db.Departamentos.AddRange(
            new Departamento
            {
                Descricao = "Departamento 01",
                Funcionarios = new List<Funcionario>
                {
                            new Funcionario
                            {
                                Nome = "Funcionario 03",
                                CPF = "99999999911",
                                RG= "2100062"
                            }
                }
            },
            new Departamento
            {
                Descricao = "Departamento 02",
                Funcionarios = new List<Funcionario>
                {
                            new Funcionario
                            {
                                Nome = "Funcionario 01",
                                CPF = "88888888811",
                                RG= "3100062"
                            },
                            new Funcionario
                            {
                                Nome = "Funcionario 02",
                                CPF = "77777777711",
                                RG= "1100062"
                            }
                }
            });

        db.SaveChanges();
        db.ChangeTracker.Clear();
    }
}